package org.academiadecodigo.personalfinance;

public class BankAccount {
    // PROPERTIES

    private double balance = 0;

    // CONSTRUCTORS

    public BankAccount() {

    }

    public BankAccount(double balance) {
        this.balance = balance;
    }

    // METHODS

    public double getBalance() {
        return balance;
    }

    public void deposit(double amount) {
        balance += amount;
    }

    public void withdraw(double amount) {
        balance -= amount;
    }
}
