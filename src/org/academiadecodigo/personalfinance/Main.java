package org.academiadecodigo.personalfinance;

public class Main {
    public static void main(String[] args) {
        User bob = new User();
        User jim = new User(10, 500);

        jim.displayAllBalances();
        jim.transferToWallet(-100);
        jim.transferToBank(-23);
        jim.displayAllBalances();
        jim.stealMoney(-59.76);
        jim.displayAllBalances();
        jim.makePurchase(Outgoings.DINNER);
        jim.displayAllBalances();

    }
}
