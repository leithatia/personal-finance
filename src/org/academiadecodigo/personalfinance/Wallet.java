package org.academiadecodigo.personalfinance;

public class Wallet {
    // PROPERTIES

    private double balance = 0;


    // CONSTRUCTORS

    public Wallet() {

    }

    public Wallet(double balance) {
        this.balance = balance;
    }

    // METHODS

    public double getBalance() {
        return balance;
    }

    public void deposit(double amount) {
        balance += amount;
    }

    public void withdraw(double amount) {
        balance -= amount;
    }

    public void addToWallet(double amount) {
        balance += amount;
    }

    public void removeFromWallet(double amount) {
        balance -= amount;
    }
}
