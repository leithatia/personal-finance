package org.academiadecodigo.personalfinance;

public enum Outgoings {
    DINNER("dinner",45),
    BEER("beer",14.00),
    HOLIDAY("holiday", 1200);

    private String name;
    private final double price;

    Outgoings(String name, double price) {
        this.name = name;
       this.price = price;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }
}
