package org.academiadecodigo.personalfinance;

public class User {
    // PROPERTIES

    private Wallet wallet;
    private BankAccount bankAccount;

    // CONSTRUCTORS

    public User() {
        wallet = new Wallet();
        bankAccount = new BankAccount();
    }

    public User(double walletInitialTotal, double bankInitialTotal) {
        wallet = new Wallet(walletInitialTotal);
        bankAccount = new BankAccount(bankInitialTotal);
    }

    // METHODS

    public void displayAllBalances() {
        System.out.printf("You have $%.2f in your wallet.\n", wallet.getBalance());
        System.out.printf("You have $%.2f in your bank.\n\n", bankAccount.getBalance());

        if (wallet.getBalance() == 0 && bankAccount.getBalance() == 0) {
            System.out.println("\nYou need to get your broke ass a job!\n\n");
        }
    }

    public void transferToBank(double amount) {
        if (amount < 0) {
            System.out.println("No no no. Can't transfer zero or a negative amount!\n");
            return;
        }

        System.out.printf("Transferring $%.2f to your bank account...\n\n", amount);

        if (amount > wallet.getBalance()) {
            System.out.println("Na uh... your wallet ain't as fat as you think it is.\n\n");
            return;
        }

        bankAccount.deposit(amount);
        wallet.withdraw(amount);

        System.out.printf("You have transferred $%.2f from your wallet to your bank account.\n\n", amount);
    }

    public void transferToWallet(double amount) {
        if (amount < 0) {
            System.out.println("No no no. Can't transfer zero or a negative amount!\n");
            return;
        }

        System.out.printf("Transferring $%.2f to your wallet...\n\n", amount);

        if (amount > bankAccount.getBalance()) {
            System.out.println("Insufficient funds. Get your broke ass a job.\n\n");
            return;
        }

        wallet.deposit(amount);
        bankAccount.withdraw(amount);

        System.out.printf("You have transferred $%.2f from your bank account to your wallet.\n\n", amount);
    }

    public void stealMoney(double amount) {
        if (amount < 0) {
            System.out.println("Great job. You attempted to mug an elderly woman but she had no cash.");
            if (wallet.getBalance() > Math.abs(amount)) {
                System.out.printf("You ended up giving her $%.2f.\n\n", Math.abs(amount));
                wallet.removeFromWallet(Math.abs(amount));
            }
        } else {
            System.out.printf("Naughty, very naughty. You mugged an elderly lady and put her $%.2f in your wallet.\n\n", amount);

            wallet.addToWallet(amount);
        }
    }

    public void makePurchase(Outgoings outgoing) {
        if (outgoing.getPrice() > wallet.getBalance()) {
            System.out.println("Hahaha... and to think you could afford a " + outgoing.getName() + ".\n\n—");
        } else {
            System.out.printf("You just spent $%.0f on %s.\n\n", outgoing.getPrice(), outgoing.getName());
            wallet.removeFromWallet(outgoing.getPrice());
        }
    }
}